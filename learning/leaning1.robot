*** Settings ***
Library     Collections
Library     Screenshot
Library     resource_python/sub.py


*** Test Cases ***
常规的关键字
    [Documentation]    常规的关键字
    log    hardwork    # 打印
    log to console    workhard    # 打印
    # 定义变量
    ${a}    Set Variable    100
    log    ${a}
    # 获取系统时间
    ${time}    get time
    log    ${time}
    log    message, level=INFO
    # 字符串拼接
    ${str}    Catenate    ni    hao    ma
    log    ${str}
    ${str}    Catenate    SEPARATOR=&    ni    hao    ma
    log    ${str}
    # 创建列表
    ${list}    Create List    ni    hao    ma
    log    ${list}
    @{list}    Create List    ni    hao    ma
    log many    @{list}
    # 字典关键字
    ${dic}    create dictionary    name=jie    age=111
    log    ${dic}
    ${value}    get from dictionary    ${dic}    name
    log    ${value}

复杂的关键字
    [Documentation]    复杂的关键字
    # 执行python方法
    ## 获取随机数
    ${rand}    EVALUATE    random.randint(1,101)    modules=random
    log    ${rand}
    ## 时间模块
    ${rand}    EVALUATE    time.time()    modules=time
    # 执行py文件里的方法
    ## 自定义一个py文件，调用add 函数。 使用import library导入
    import library    C:/Users/j7li/OneDrive/gitlab.com/jageli/robot/learning/resource_python/add.py
    ${sum}    add    ${10}    ${11}
    ## 自定义一个py文件，调用sub函数。使用Library在Settings里导入
    ${sub}    sub    ${10}    ${11}

流程控制
    [Documentation]    流程控制
    # if 语句: 注意ELSE IF 和 ELSE 需要大写
    ${a}    set variable    90
    IF    ${a} < 60
        log    不及格
    ELSE IF    ${a} < 80
        log    一般
    ELSE
        log    优秀
    END
    # FOR 语句    注意大写. 写法1
    FOR    ${a}    IN    aa    bb    cc
        log    ${a}
    END
    # FOR 语句    注意大写. 写法2
    ${list}    Create List    aa1    bb1    cc1
    FOR    ${a}    IN    ${list}
        log    ${a}
    END
    @{list}    Create List    aa2    bb2    cc2
    FOR    ${a}    IN    @{list}
        log    ${a}
    END
    # FOR 语句    注意大写. 写法3
    FOR    ${a}    IN RANGE    1    11
        IF    ${a}==6    BREAK
        log    ${a}
    END
    # 截图
    take screenshot
