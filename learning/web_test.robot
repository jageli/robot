*** Settings ***
Library     SeleniumLibrary


*** Test Cases ***
常用关键字
    [Documentation]    常用关键字
    #打开浏览器并加载网页
    Open Browser    https://www.baidu.com    firefox
    # 设置隐式等待
    Set Browser Implicit Wait    3
    # 设置浏览器最大化
    sleep    1
    maximize browser window
    # 指定浏览器的大小
    sleep    1
    set window size    800    600
    # 获取浏览器的高度和宽度
    ${width}    ${heigth}    get window size
    # 回退
    sleep    1
    go back
    # 前进
    sleep    1
    go to    https://fanyi.baidu.com/translate?aldtype=16047&query=&keyfrom=baidu&smartresult=dict&lang=auto2zh#auto/zh/
    # 刷新
    sleep    1
    reload page
    # 获得标题
    ${title}    get title
    # 获得浏览器地址
    ${location}    get location
    # 关闭浏览器
    close browser

元素定位
    [Documentation]    元素定位

    open browser    https://www.baidu.com    firefox
    set browser implicit wait    3

    # id定位
    comment    input text    id=kw    周杰

    # name定位
    comment    input text    name=wd    周杰伦

    # link text定位
#    click element    link=图片

    # partial_link_text 部分连接文本定位
    click element    partial link=新闻

#    click element    id=su

    [Teardown]    Close Browser
